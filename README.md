![NKUA logo](Markdown/athena.svg)  
National and Kapodistrian University of Athens  
School of Sciences  
Department of Informatics and Telecommunications  
Program of Postgraduate Studies  
PhD Thesis

# Constraint Programming: Algorithms and Systems

__Nikolaos Pothitos__

Supervised by Prof. __Panagiotis Stamatopoulos__

## Abstract

Constraint Programming aims at the easy declaration and fast
resolution of Constraint Satisfaction Problems (CSPs) like
course scheduling, radio link frequency assignment, etc. To
solve the problems, Constraint Programming is based on

  + search methods and
  + constraint propagation.

This dissertation contributes on both. Specifically:

 1. We develop novel search methods that are based on new
    heuristics. These new heuristics implement the _gradual_
    randomization of deterministic heuristics. We create
    hybrid heuristics that exploit the advantages of both
    deterministic and random heuristics.

 2. We demonstrate how the MapReduce framework can be used
    for speeding up and distributing the search of a CSP
    solution to all the available solvers-workers.

 3. We highlight the advantages of relaxed constraint
    propagation levels like _bounds consistency_ in
    comparison to higher levels like _arc consistency_. We
    propose new relaxed constraint propagation levels, and
    we compare their performance to higher propagation
    levels, both in theory and practice. We answer the
    question about when it is worth to employ relaxed
    constraint propagation levels.

Our contributions were tested using mostly CSPs that occur
in the real world and a wide range of CSPs included in
official Constraint Programming solvers competitions. We
used Naxos Solver as a practical open-source Constraint
Programming solver to conduct our experiments.

## Acknowledgments

> Aren't we too grown for games?  
> Aren't we too grown to play around?  
> Young enough to chase  
> But old enough to know better
> 
> Maroon 5 – [What lovers do ft. SZA](https://youtu.be/5Wiio4KoGe8)

Back in the '90s when I was kid, we visited with my school
the Geology Museum of the University of Athens. I still
remember the awe I felt in front of the big buildings. At
that time, it seemed to me like an elusive dream to be just
an undergraduate student. I could not even imagine that a
few decades later I would be defending my doctoral
dissertation!

I am deeply grateful to Prof. Panagiotis Stamatopoulos for
our strong and close cooperation for more than half of my
life. Thanks for the empathy, generosity, motivation,
support in scientific, technical, and personal level!

I would like to thank my Three-member Advisory Committee
Professors Constantin Halatsis and Stavros Kolliopoulos and
the Seven-member Examination Committee. Special thanks to
Prof. Ilias Sakellariou for his numerous and valuable
comments and corrections.

I express my most sincere appreciation to Prof. Isambo
Karali and Dr. Anastasia Paparrizou for their advice
throughout this journey. I also really appreciate the
contribution of Ektoras Tavoularis who updated the
Continuous Integration system of our Constraint Programming
_Naxos Solver_.

Many thanks to Dr. Nikos Raptis and my “Nokian” colleagues
for their support, fun, and encouragement they generously
offered to me always!

I would like to thank Bodossaki Foundation and Ioannis
Detsis, Georgios Bibilas, Ioannis Mathioudakis, Sotiris
Laganopoulos, and Dr. Eleni Detsi for their scholarship
during the dark years of the Greek crisis. Special thanks to
Christos Karatsalos who informed me about the scholarship;
without his encouragement I would never have applied.

I cannot forget the warm hospitality by Christos Palantzas
during my stay in Lamia for the 7<sup>th</sup> Hellenic
Artificial Intelligence conference.

I grab the opportunity to express my total support and
solidarity with Dr. Timnit Gebru and Dr. Margaret Mitchell,
the two leaders of Google Ethical AI research that were
recently fired. We have a long way to go until corporate AI
becomes ethical.

I would like to thank my parents Yannis and Elli and my
sister Evi.

Many thanks to my wife Matina-Maria and my children Ioannis,
Thomas, and Maria-Varvara! They all came into my life after
beginning this dissertation, so I dedicate it to them! I
love you!

Finally, I would like to thank the emeritus professor Harold
Cohen (1928–2016) who sent to me two wonderful paintings,
Fig. [1](#fig1-harold-cohen-090921-print) and
[1.1](#fig11-harold-cohen-040501-print), designed using
Artificial Intelligence. Just for the record, I quote the
permission that he granted me. Rest in peace.

###### Fig. 1: Harold Cohen, _090921_, print

![090921](Markdown/090921.jpg)

> Date: Sat, 15 Oct 2011 09:05:11 -0400  
> From: Harold Cohen \<hcohen@ucsd.edu\>
> 
> Dear Mr. Pothitos,
> 
> Hope these will serve your needs. Please understand that
> they may not be used in any context other than your PhD
> thesis.
> 
> For captions: these are both prints; the “titles” are
> actually the dates in `ymd` format.
> 
> Best wishes,  
> Harold Cohen

## Introduction

> None are more hopelessly enslaved than those who falsely
> believe they are free.  
> Johann Wolfgang von Goethe

With this quote, Goethe implies that we are all under strict
_constraints_; they are an integral part of our lives, even
when we do not admit it.

Therefore, a good approach to tackle any problem is to
explicitly describe its constraints and search for a
solution that does not violate them.

### What is Constraint Programming?

Here Constraint Programming comes into the picture. Its
motto is “the user simply states the problem and the
computer solves it.” This proposition implies that

  + the “user” is required to provide only a bare minimum of
    the description of a problem, i.e. only the
    _constraints_ should be defined, and

  + the rest (solution search process) is undertaken by the
    machine, i.e. solver.

There are several variations of the solution search
processes under the Constraint Programming umbrella. In all
cases, however, we describe and formalize every problem as a
_Constraint Satisfaction Problem_ (CSP). A formal
description of a CSP will follow in the next chapter.

Conclusively, Constraint Programming is the set of all the
methodologies that can solve arbitrary Constraint
Satisfaction Problems (CSPs).

The description of any CSP is a minimal definition of _what_
the problem is and does _not_ contain information on _how_
to solve it. Normally, a CSP has a very large number of
candidate solutions, and a Constraint Programming
methodology should be able to identify the feasible
solutions out of them.

Constraint Programming allows the easy and declarative
statement of a CSP and provides an “armory” of several
generic search methods that can be used to solve it.
Constraint Programming has been applied in scheduling, radio
frequency assignment, Bioinformatics problems, etc.

### How Constraint Programming relates to AI?

_Artificial Intelligence_ (AI) is a prestigious Computer
Science area that changes the world. Distinguished AI
applications include self-driving cars, search engines,
medical diagnosis, image recognition, even automatic drawing
of paintings such as the ones illustrated in
Fig. [1](#fig1-harold-cohen-090921-print) and
[1.1](#fig11-harold-cohen-040501-print).

###### Fig. 1.1: Harold Cohen, _040501_, print

![040501](Markdown/040501.jpg)

Computer vision is a discipline that traditionally belongs
to AI. In 1975, David Waltz introduced constraint
propagation, the core of Constraint Programming, to create a
three-dimensional view of an object given a two-dimensional
image. Two years later, Alan Mackworth published in the
journal of _Artificial Intelligence_ the evolution of this
constraint propagation algorithm which is with variations
still the heart of most Constraint Programming solvers.

Constraint Programming also adopts search methods used in AI
to solve Constraint Satisfaction Problems. A series of AI
methods like depth-first search (DFS), limited discrepancy
search (LDS), etc. have been successfully employed in the
Constraint Programming world, during the CSP solving phase.
Furthermore, Machine Learning (ML) is constantly gaining
ground in the context of Constraint Programming.

AI is only one of the areas that have contributed to
Constraint Programming so far. Integer Programming and
Linear Programming that belong to Operations Research,
another Computer Science discipline, have also influenced
Constraint Programming.

### How Constraint Programming relates to programming?

When one hears the term “Constraint Programming” for the
first time, they often imagine that it is something like a
common programming language. Nevertheless, the word
“programming” here has a broader meaning.

Historically, Constraint Programming was implemented for the
first time using Logic Programming. Thus, only the
“Constraint Logic Programming” term initially existed.

When different implementations (imperative languages) came
into the picture, the generalized term “Constraint
Programming” was introduced.

Constraint Programming should not be mistaken for a tool to
describe the steps toward solving a problem. Constraint
Programming is all about describing the constraints that a
solution should satisfy. Searching for a solution is done
behind the scenes.

### Our contributions

In programming languages, the statement of an algorithm is
followed by compilation/interpretation and execution. In
Constraint Programming, the statement of the constraints is
followed by independent methodologies that solve the
problem. This dissertation aims to make the solving process
more efficient.

The next Chapter 2 contains the preliminaries needed to
understand our contributions in the rest of the chapters.
Constraint Satisfaction Problems are formally defined along
with a framework of search methods that solve them by
employing heuristics. Constraint propagation, a basic
element of Constraint Satisfaction, is introduced.

Chapter 3 goes through the papers that are relevant to our
contributions and presents MapReduce.

Chapter 4 illustrates our first contribution. We study two
distinct categories of heuristics, deterministic and random,
and we make the most out of them by smoothly combining them.
We create new hybrid heuristics and a new search method
based on them.

In Chapter 5, we integrate Constraint Programming into the
state-of-the-art distributed MapReduce framework. We exploit
MapReduce scalability to solve large CSP instances.

Finally, in Chapter 6, we highlight the advantages of
relaxed constraint propagation methodologies when used in
conjunction with search methods to validate constraints and
speed up search. This last contribution attempts to
contradict the “conventional wisdom” which implies that arc
consistency or even higher consistency levels are always
better than bounds consistency. We propose a new relaxed
consistency level and a criterion to decide a priori when to
enforce relaxed consistency instead of higher consistency
levels.

### 👉 Read the full PhD Thesis [PDF](http://cgi.di.uoa.gr/~pothitos/thesis.pdf)

The presentation
[slides](http://cgi.di.uoa.gr/~pothitos/presentation.pdf)
and a summary in [Greek](Markdown/Greek.md) are also
available.
